# Sistema Domótico para el control de una vivienda

Esto proyecto servira como guía para la implementación de un sistema domótico
para una vivienda, el sistema estara compuesto por varios nodos que a su vez pueden
tener varios sensores y actuadores, estos nodos usan la red de MySensors para enviar
información de su estado a un Gateway el cual hace de intermediario entre la red y
un controlador. Esta información seria leida por el controlador de Domoticz el cual servira de interfaz de usuario del sistema.


En el documento PDF de este repositorio vienen detallados los materiales necesarios para montar el sistema junto con el codigo de cada nodo.


Documentación de domoticz  https://domoticz.com/
Documentación de mysensors https://www.mysensors.org/
