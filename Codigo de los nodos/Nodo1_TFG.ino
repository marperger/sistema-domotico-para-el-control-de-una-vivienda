
// Enable debug prints
#define MY_DEBUG
#define MY_NODE_ID 142
// Enable and select radio type attached
#define MY_RADIO_NRF24
#define MY_RF24_CHANNEL  81

#define MY_SOFTSPI

#define MY_RF24_CE_PIN 8
#define MY_RF24_CS_PIN 7
#define MY_SOFT_SPI_SCK_PIN 6
#define MY_SOFT_SPI_MISO_PIN 4
#define MY_SOFT_SPI_MOSI_PIN 5

//activa los mensajes de depuración relacionados con la seguridad.
#define MY_DEBUG_VERBOSE_SIGNING
//indica que usaremos la solución por software.
#define MY_SIGNING_SOFT
//el algoritmo que firma el mensaje necesita de un numero aleatorio, el número será generado a partir del pin que hayamos definido.
#define MY_SIGNING_SOFT_RANDOMSEED_PIN 7
//indica que el nodo solo va a recibir mensaje que estén firmados.
#define MY_SIGNING_REQUEST_SIGNATURES 

#include <MySensors.h>
#include <SPI.h>
#include <MFRC522.h>


#define RF_INIT_DELAY   125
#define ONE_SEC         1000
#define MAX_CARDS       18
#define PROG_WAIT       10
#define HEARTBEAT       10
#define BAUD            115200

/*Pin definitions*/
#define LEDVERDE         14
#define PIR_SENSOR 3  // The digital input you attached your motion sensor.  (Only 2 and 3 generates interrupt!)
#define SOLENOIDE 16
#define LEDROJO    15   //  MFRC 

#define RST_PIN   10   //  MFRC 
#define SS_PIN    9   //  MFRC 

MFRC522      mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
MFRC522::Uid olduid;
MFRC522::Uid masterkey = { 4, { 0x2A, 0xD5, 0x20, 0xBE},  0 };

byte       countValidCards = 0;
MFRC522::Uid validCards[MAX_CARDS];

void       ShowCardData(MFRC522::Uid* uid);
bool       sameUid(MFRC522::Uid* old, MFRC522::Uid* check);
void       copyUid(MFRC522::Uid* src, MFRC522::Uid* dest);
bool       isValidCard(MFRC522::Uid* uid);
int        releasecnt = 0;

#define CHILD_ID_LOCK 8
#define CHILD_ID_PIR 9   // Id of the sensor child
#define CHILD_ID_SOLENOIDE 10   // Id of the sensor child

int        oldSwitchValue = -1;
int        switchValue = 0;
long       timer = -1;
bool       programmode = false;
bool       ledon;
int        programTimer = 0;
bool       armed = true;
unsigned long lastTime = 0;

bool tripped = false;

MyMessage  lockMsg(CHILD_ID_LOCK, V_LOCK_STATUS);

MyMessage pirMsg(CHILD_ID_PIR, V_TRIPPED);

void before() {
  // Make sure MFRC is disabled from the SPI bus
  pinMode(RST_PIN, OUTPUT);
  digitalWrite(RST_PIN, LOW);
  pinMode(SS_PIN, OUTPUT);
  digitalWrite(SS_PIN, LOW);
}

void presentation() {
  sendSketchInfo("CERRADURA ELECTRONICA", "1.0"); delay(RF_INIT_DELAY);
  // Register all sensors to gw (they will be created as child devices)

  present(CHILD_ID_LOCK, S_LOCK);      delay(RF_INIT_DELAY);

  present(CHILD_ID_PIR, S_MOTION);
}

void setup() {
  Serial.begin(BAUD);   // Initialize serial communications with the PC

  // Make sure MFRC will be disabled on the SPI bus
  /*pinMode(RST_PIN, OUTPUT);
    digitalWrite(RST_PIN, LOW);
    pinMode(SS_PIN, OUTPUT);
    digitalWrite(SS_PIN, LOW);*/
  pinMode(RST_PIN, OUTPUT);
  digitalWrite(RST_PIN, LOW);
  pinMode(SS_PIN, OUTPUT);
  digitalWrite(SS_PIN, LOW);

  pinMode(PIR_SENSOR, INPUT);      // sets the motion sensor digital pin as input
  pinMode(SOLENOIDE, OUTPUT);

  pinMode(LEDVERDE, OUTPUT);
  digitalWrite(LEDVERDE, LOW);
  pinMode(LEDROJO, OUTPUT);
  digitalWrite(LEDROJO, LOW);
  // Setup the button
  // Activate internal pull-up

  // After setting up the button, setup debouncer

  // Init mysensors library

  /*sendSketchInfo("RFID Garage", "1.1"); delay(RF_INIT_DELAY);

    // Register all sensors to gw (they will be created as child devices)
    present(CHILD_ID_LOCK, S_LOCK);      delay(RF_INIT_DELAY);
    present(CHILD_ID_ALARM, S_MOTION);   delay(RF_INIT_DELAY);*/

  recallEeprom();

  // Init MFRC RFID sensor
  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522
  ShowReaderDetails();          // Show details of PCD - MFRC522 Card Reader details

  Serial.println(F("Init done..."));
}

bool trippedPast;
void loop() {
  timer++;
  delay(HEARTBEAT);
  
  tripped = digitalRead(PIR_SENSOR) == HIGH;
  if (trippedPast != tripped && ((timer % (ONE_SEC / HEARTBEAT)) == 0)) {
  
      
    send(pirMsg.set(tripped ? "1" : "0"));
    trippedPast = tripped;
  }
 
  if (programmode && ((timer % (ONE_SEC / HEARTBEAT)) == 0)) {
    ledon = !ledon;
    digitalWrite(LEDVERDE, ledon);
    digitalWrite(LEDROJO, !ledon);
    programTimer++;

    // Stop program mode after 20 sec inactivity
    if (programTimer > PROG_WAIT) {
      programmode = false;
      digitalWrite(LEDVERDE, false);
      digitalWrite(LEDROJO, false);
      Serial.println(F("Program expired"));
    }
  }

  if ((timer % (200 / HEARTBEAT)) == 0) {
    // Look for new cards
    if (!mfrc522.PICC_IsNewCardPresent()) {
      if (releasecnt > 0) {
        releasecnt--;
        if (!releasecnt) {
          olduid.size = 0;
          Serial.println(F("release"));
        }
      }
      return;
    }
    releasecnt = 5;

    // Select one of the cards
    if (!mfrc522.PICC_ReadCardSerial()) {
      return;
    }

    // Dump debug info about the card; PICC_HaltA() is automatically called
    //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
    if (!olduid.size || !sameUid(&(mfrc522.uid), &olduid)) {
      ShowCardData(&(mfrc522.uid));
      copyUid(&(mfrc522.uid), &olduid);
      if (isValidCard(&olduid)) {
        OpenDoor(programmode);
      }
      else {

        if (sameUid(&(mfrc522.uid), &masterkey)) {
          // Only switch in program mode when mastercard is found AND the program button is pressed
          Serial.println(F("Program mode"));
          programmode = true;
          programTimer = 0;
          lastTime = 0;
        }
        else {
          if (programmode) {
            Serial.println(F("new card"));
            programTimer = 0;

            if (countValidCards < MAX_CARDS)
            {
              // Add card to list...
              copyUid(&(mfrc522.uid), &validCards[countValidCards]);
              countValidCards++;
              blinkFast(15);
              storeEeprom();
            }
          }

          else {
            Serial.println(F("Invalid card"));
            digitalWrite(LEDROJO, HIGH);

            delay(1000);
            digitalWrite(LEDROJO, LOW);

          }
        }
      }
    }
  }
}

void ShowCardData(MFRC522::Uid* uid) {
  Serial.print(F("Card UID:"));
  for (byte i = 0; i < uid->size; i++) {
    if (uid->uidByte[i] < 0x10) {
      Serial.print(F(" 0"));
    }
    else {
      Serial.print(F(" "));
    }
    Serial.print(uid->uidByte[i], HEX);
  }
  Serial.println();
}

void copyUid(MFRC522::Uid* src, MFRC522::Uid* dest)
{
  dest->size = src->size;
  dest->sak = src->sak;

  for (byte i = 0; i < src->size; i++) {
    dest->uidByte[i] = src->uidByte[i];
  }
}

bool sameUid(MFRC522::Uid* old, MFRC522::Uid* check)
{
  if (old->size != check->size) {
    return false;
  }
  for (byte i = 0; i < old->size; i++) {
    if (old->uidByte[i] != check->uidByte[i]) {
      return false;
    }
  }
  return true;
}

bool isValidCard(MFRC522::Uid* uid)
{
  for (byte i = 0; i < countValidCards; i++) {
    if (validCards[i].size != uid->size) {
      break;
    }
    for (int j = 0; j < uid->size; j++) {
      if (validCards[i].uidByte[j] != uid->uidByte[j]) {
        break;
      }
      if (j == (uid->size - 1)) {
        return true;
      }
    }
  }
  return false;
}


void storeEeprom()
{
  byte address = 0;
  saveState(address++, countValidCards);

  for (byte i = 0; i < countValidCards; i++) {
    saveState(address++, validCards[i].size);
    for (byte j = 0; j < 10; j++) {
      saveState(address++, validCards[i].uidByte[j]);
    }
  }
}

void recallEeprom()
{
  byte address = 0;

  countValidCards = loadState(address++);
  if (countValidCards > MAX_CARDS) {
    Serial.println(F("Not a valid EEPROM reading set to default"));
    countValidCards = 0;
    storeEeprom();
    return;
  }

  for (byte i = 0; i < countValidCards; i++) {
    validCards[i].size = loadState(address++);
    for (byte j = 0; j < 10; j++) {
      validCards[i].uidByte[j] = loadState(address++);
    }
  }

}

void blinkFast(int times)
{
  for (int i = 0; i < times; i++) {
    ledon = !ledon;
    digitalWrite(LEDVERDE, ledon);
    delay(100);
  }
}

void OpenDoor(bool fakeOpen)
{

  Serial.println(F("Open door!"));
  send(lockMsg.set(false));

  if (!fakeOpen) {
    digitalWrite(LEDVERDE, HIGH);
    delay(200);
    digitalWrite(LEDVERDE, LOW);
  }

  controlSolenoide();


  send(lockMsg.set(true));
}

void ShowReaderDetails() {
  // Get the MFRC522 software version
  byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
  Serial.print(F("MFRC522 Software Version: 0x"));
  Serial.print(v, HEX);
  if (v == 0x91) {
    Serial.print(F(" = v1.0"));
  }
  else if (v == 0x92) {
    Serial.print(F(" = v2.0"));
  }
  else {
    Serial.print(F(" (unknown)"));
  }
  Serial.println("");

  // When 0x00 or 0xFF is returned, communication probably failed
  if ((v == 0x00) || (v == 0xFF)) {
    Serial.println(F("WARNING: Communication failure, is the MFRC522 properly connected?"));
  }
}

void controlSolenoide() {
  send(lockMsg.set(LOW));
  digitalWrite(SOLENOIDE, HIGH);
  digitalWrite(LEDVERDE, HIGH);
  smartSleep(2000);
  digitalWrite(SOLENOIDE, LOW);
  digitalWrite(LEDVERDE, LOW);
  send(lockMsg.set(HIGH));
}
void receive(const MyMessage &message)
{
  if (message.type == V_LOCK_STATUS) {
    // Change relay state
    if (!message.getBool()) {
      OpenDoor(false);
    }

    // Write some debug info
    Serial.print(F("Lock status: "));
    Serial.println(message.getBool());
  }
  else
  {
    // Write some debug info
    Serial.print(F("Incoming msg type: "));
    Serial.print(message.type);
    Serial.print(F(" id: "));
    Serial.print(message.sensor);
    Serial.print(F(" content: "));
    Serial.println(message.getInt());
  }
}
