//Activa mensajes de depuracion de Mysensrs
#define MY_DEBUG

// Activar la radio y configuracion de sus pines
#define MY_RADIO_NRF24
#define MY_RF24_CE_PIN 14
#define MY_RF24_CS_PIN 10
#define MY_RF24_CHANNEL 81 // Indicamos el canal de comunicacion
#define MY_NODE_ID 150 //Numero de identificacion del nodo

//activa los mensajes de depuración relacionados con la seguridad.
#define MY_DEBUG_VERBOSE_SIGNING
//indica que usaremos la solución por software.
#define MY_SIGNING_SOFT
//el algoritmo que firma el mensaje necesita de un numero aleatorio, el número será generado a partir del pin que hayamos definido.
#define MY_SIGNING_SOFT_RANDOMSEED_PIN 7
//indica que el nodo solo va a recibir mensaje que estén firmados.
#define MY_SIGNING_REQUEST_SIGNATURES 

//Inclusion de librerias
#include <MySensors.h>
#include "BMP280.h"


//definicion de constantes para el control de la bateria y del sensor
int oldBatteryPcnt = 0;
#define FULL_BATTERY 4.2 
#define P0 1010.6 
#define CHILD_ID_TEMP 1
#define CHILD_ID_PRESS 2
#define SLEEPTIME 60000
#define CHILD_ID_BAT V_VOLTAGE

//Variables necesarias para el algoritmo de prediccion de clima
const char *weather[] = { "stable", "sunny", "cloudy", "unstable", "thunderstorm", "unknown" };
enum FORECAST
{
  STABLE = 0,      // "Stable Weather Pattern"
  SUNNY = 1,      // "Slowly rising Good Weather", "Clear/Sunny "
  CLOUDY = 2,     // "Slowly falling L-Pressure ", "Cloudy/Rain "
  UNSTABLE = 3,   // "Quickly rising H-Press",     "Not Stable"
  THUNDERSTORM = 4, // "Quickly falling L-Press",    "Thunderstorm"
  UNKNOWN = 5     // "Unknown (More Time needed)
};

//variables de control
BMP280 bmp; 
float lastPressure = -1;
float lastTemp = -1;
int lastForecast = -1;
const int LAST_SAMPLES_COUNT = 5;
float lastPressureSamples[LAST_SAMPLES_COUNT];

#define CONVERSION_FACTOR (1.0/10.0) //factor de conversion de presion atmosferica
int minuteCount = 0;
bool firstRound = true;
float pressureAvg;
float pressureAvg2;
float dP_dt;

// Mensajes del nodo 
MyMessage msgT1(CHILD_ID_TEMP, V_TEMP);
MyMessage msgP1(CHILD_ID_PRESS, V_PRESSURE);
MyMessage msgF1(CHILD_ID_PRESS, V_FORECAST);
MyMessage voltMsg(CHILD_ID_BAT, V_VOLTAGE);


//presentacion de los componentes del nodo
void presentation()
{
  sendSketchInfo("Nodo movil medioambiental", "1.1");
  present(CHILD_ID_TEMP, S_TEMP);
  present(CHILD_ID_PRESS, S_BARO);
  present(CHILD_ID_BAT, S_MULTIMETER);
}

void setup()
{
  //esperamos 0.5 s para que se incialice el sensor
  delay(500);

  //inicializamos el sensor
  if (!bmp.begin())
  {
    Serial.println("BMP init failed!");
    while (1);
  }
  else Serial.println("BMP init success!");
  bmp.setOversampling(4);
  ServerUpdate(); 
}

void loop()
{ 
  //Dormimos el nodo por 1 min
  smartSleep(SLEEPTIME); 
  //control de la bateria
  battery();
  //control del sensor
  ServerUpdate();
}

//Funcion de control del sensor BME280
void ServerUpdate() // used to read sensor data and send it to controler
{
  double T, P;
  //iniciamos la lectura del sensor
  char result = bmp.startMeasurment();

  //si no se ha producido un error procedemos a leer la temperatura y la presion
  if (result != 0) {
    delay(result);
    //leemos la temperatura y la presion
    result = bmp.getTemperatureAndPressure(T, P);

    //hacemos una prediccion del tiempo
    int forecast = sample(P);

    //si la lectura es exitosa enviamos los datos leido al gateway
    if (result != 0)
    {
      send(msgT1.set(T, 1));
      send(msgP1.set(P, 1));
      send(msgF1.set(weather[forecast]));
    }
    else {
      //si la lectura fallo imprimimos un error
      Serial.println("Error.");
    }
  }
  else {
    //si la lectura fallo imprimimos un error
    Serial.println("Error.");
  }
}
//Funcion para obtener la media de las presiones leidas
float getLastPressureSamplesAverage()
{
  float lastPressureSamplesAverage = 0;
  for (int i = 0; i < LAST_SAMPLES_COUNT; i++)
  {
    lastPressureSamplesAverage += lastPressureSamples[i];
  }
  lastPressureSamplesAverage /= LAST_SAMPLES_COUNT;

  return lastPressureSamplesAverage;
}

//funcion para calcular el valor actual de la bateria y su porcentaje
void battery() {
  //se obtiene la tension de la bateria en milivoltios
  long batteryMillivolts = hwCPUVoltage();
  //se calcula el porcentaje de la bateria
  int batteryPcnt = batteryMillivolts / FULL_BATTERY / 1000.0 * 100 + 0.5;
#ifdef MY_DEBUG
  Serial.print("Battery voltage: ");
  Serial.print(batteryMillivolts / 1000.0);
  Serial.println("V");
  Serial.print("Battery percent: ");
  Serial.print(batteryPcnt);
  Serial.println(" %");
#endif

  //si el porcentaje de la bateria a cambiado desde la ultima lectura se lo
  //envia al gateway
  if (oldBatteryPcnt != batteryPcnt) {
    sendBatteryLevel(batteryPcnt);
    oldBatteryPcnt = batteryPcnt;
  }
  //Se envia la tension actual de la bateria
   send(voltMsg.set(batteryMillivolts / 1000.0 ,2));
}

//Algoritmo para la prediccion del clima, enlace del algoritmo se encuentra
//en la bibliografia del documento
int sample(float pressure)
{
  // Calculate the average of the last n minutes.
  int index = minuteCount % LAST_SAMPLES_COUNT;
  lastPressureSamples[index] = pressure;

  minuteCount++;
  if (minuteCount > 185)
  {
    minuteCount = 6;
  }

  if (minuteCount == 5)
  {
    pressureAvg = getLastPressureSamplesAverage();
  }
  else if (minuteCount == 35)
  {
    float lastPressureAvg = getLastPressureSamplesAverage();
    float change = (lastPressureAvg - pressureAvg) * CONVERSION_FACTOR;
    if (firstRound) // first time initial 3 hour
    {
      dP_dt = change * 2; // note this is for t = 0.5hour
    }
    else
    {
      dP_dt = change / 1.5; // divide by 1.5 as this is the difference in time from 0 value.
    }
  }
  else if (minuteCount == 65)
  {
    float lastPressureAvg = getLastPressureSamplesAverage();
    float change = (lastPressureAvg - pressureAvg) * CONVERSION_FACTOR;
    if (firstRound) //first time initial 3 hour
    {
      dP_dt = change; //note this is for t = 1 hour
    }
    else
    {
      dP_dt = change / 2; //divide by 2 as this is the difference in time from 0 value
    }
  }
  else if (minuteCount == 95)
  {
    float lastPressureAvg = getLastPressureSamplesAverage();
    float change = (lastPressureAvg - pressureAvg) * CONVERSION_FACTOR;
    if (firstRound) // first time initial 3 hour
    {
      dP_dt = change / 1.5; // note this is for t = 1.5 hour
    }
    else
    {
      dP_dt = change / 2.5; // divide by 2.5 as this is the difference in time from 0 value
    }
  }
  else if (minuteCount == 125)
  {
    float lastPressureAvg = getLastPressureSamplesAverage();
    pressureAvg2 = lastPressureAvg; // store for later use.
    float change = (lastPressureAvg - pressureAvg) * CONVERSION_FACTOR;
    if (firstRound) // first time initial 3 hour
    {
      dP_dt = change / 2; // note this is for t = 2 hour
    }
    else
    {
      dP_dt = change / 3; // divide by 3 as this is the difference in time from 0 value
    }
  }
  else if (minuteCount == 155)
  {
    float lastPressureAvg = getLastPressureSamplesAverage();
    float change = (lastPressureAvg - pressureAvg) * CONVERSION_FACTOR;
    if (firstRound) // first time initial 3 hour
    {
      dP_dt = change / 2.5; // note this is for t = 2.5 hour
    }
    else
    {
      dP_dt = change / 3.5; // divide by 3.5 as this is the difference in time from 0 value
    }
  }
  else if (minuteCount == 185)
  {
    float lastPressureAvg = getLastPressureSamplesAverage();
    float change = (lastPressureAvg - pressureAvg) * CONVERSION_FACTOR;
    if (firstRound) // first time initial 3 hour
    {
      dP_dt = change / 3; // note this is for t = 3 hour
    }
    else
    {
      dP_dt = change / 4; // divide by 4 as this is the difference in time from 0 value
    }
    pressureAvg = pressureAvg2; // Equating the pressure at 0 to the pressure at 2 hour after 3 hours have past.
    firstRound = false; // flag to let you know that this is on the past 3 hour mark. Initialized to 0 outside main loop.
  }

  int forecast = UNKNOWN;
  if (minuteCount < 35 && firstRound) //if time is less than 35 min on the first 3 hour interval.
  {
    forecast = UNKNOWN;
  }
  else if (dP_dt < (-0.25))
  {
    forecast = THUNDERSTORM;
  }
  else if (dP_dt > 0.25)
  {
    forecast = UNSTABLE;
  }
  else if ((dP_dt > (-0.25)) && (dP_dt < (-0.05)))
  {
    forecast = CLOUDY;
  }
  else if ((dP_dt > 0.05) && (dP_dt < 0.25))
  {
    forecast = SUNNY;
  }
  else if ((dP_dt > (-0.05)) && (dP_dt < 0.05))
  {
    forecast = STABLE;
  }
  else
  {
    forecast = UNKNOWN;
  }

  return forecast;
}
