
// Activar la radio 
#define MY_RADIO_NRF24
#define MY_RF24_CHANNEL 81 // Indicamos el canal de comunicacion
#define MY_NODE_ID 143 //Numero de identificacion del nodo

//Activa mensajes de depuracion de Mysensrs
#define MY_DEBUG

//activa los mensajes de depuración relacionados con la seguridad.
#define MY_DEBUG_VERBOSE_SIGNING
//indica que usaremos la solución por software.
#define MY_SIGNING_SOFT
//el algoritmo que firma el mensaje necesita de un numero aleatorio, el número será generado a partir del pin que hayamos definido.
#define MY_SIGNING_SOFT_RANDOMSEED_PIN 7
//indica que el nodo solo va a recibir mensaje que estén firmados.
#define MY_SIGNING_REQUEST_SIGNATURES 
//Inclusion de librerias

#include <MySensors.h>
#include <Bounce2.h>

//definicion de constantes para el control del programa
#define CHILD_ID_LIGHT 1
#define CHILD_ID_RELAY 2 // Id del rele dentro del nodo
#define RELAY_PIN 7  // El rele esta conectado al pin 7
#define RELAY_ON 1
#define RELAY_OFF 0


// Mensajes del nodo 
MyMessage lightMsg(CHILD_ID_LIGHT, V_LIGHT);
MyMessage rgbMsg(CHILD_ID_LIGHT, V_RGB);
MyMessage dimmerMsg(CHILD_ID_LIGHT, V_DIMMER);
MyMessage msgRelay(CHILD_ID_RELAY, V_LIGHT);

//variables de control
int current_r = 255;
int current_g = 255;
int current_b = 255;
int target_r = 255;
int target_g = 255;
int target_b = 255;
int save_r;
int save_g;
int save_b;


float delta_r = 0.0;
float delta_g = 0.0;
float delta_b = 0.0;

char rgbstring[] = "ffffff";

int on_off_status = 0;
int dimmerlevel = 100;
int fadespeed = 20;
unsigned long last_update = 0;
int tick_length = 10;
int fade_step = 0;
bool state;

int program_timer;
int program_cycle;
int cont_program_only_one=0;
//Pines de la tira de leds
#define REDPIN 4
#define GREENPIN 5
#define BLUEPIN 3
//Modos de luz, normal y desvaneciente
#define LIGHT_NORMAL 0
#define LIGHT_FADING 1
//Definimos constantes para cada modo
#define PROGRAM_NORMAL 0
#define PROGRAM_ALARM 1
#define PROGRAM_RELAX 2
#define PROGRAM_ONLY_ONE 3

int light_mode = LIGHT_NORMAL;
int program_mode = PROGRAM_NORMAL;
//duracion del modo relax
#define RELAX_SPEED 50
#define MAX_CYCLES_RELAX 7
//Valores que toman los leds durante modo relax
const int program_param_RELAX[MAX_CYCLES_RELAX][3] = {
  {160, 32, 0},
  {160, 32, 16},
  {80, 16, 32},
  {80, 128, 0},
  {80, 32, 00},
  {160, 32, 32},
  {160, 0, 32}
};

void setup()
{
  // Arreglamos el timer PWM para evitar que los leds parpadeen
  TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM00);

  //Definimos como salida los pines de la tira de les y del rele
  pinMode(REDPIN, OUTPUT);
  pinMode(GREENPIN, OUTPUT);
  pinMode(BLUEPIN, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);
  
  digitalWrite(RELAY_PIN, RELAY_OFF);
  //cargamos de memoria el ultimo estado del rele guardado
  state = loadState(CHILD_ID_RELAY);
  digitalWrite(RELAY_PIN, state ? RELAY_ON : RELAY_OFF);
}
//presentacion de los componentes del nodo
void presentation()
{
  sendSketchInfo("Control de luces", "1.0");
  present(CHILD_ID_LIGHT, S_RGB_LIGHT);
  present(CHILD_ID_RELAY, S_LIGHT);
}
//funcion para inicializar la tira de leds
void selftest() {
  on_off_status = 1;
  current_r = 255;
  current_g = 0;
  current_b = 0;
  set_hw_status();
  wait(100);
  current_r = 0;
  current_g = 255;
  set_hw_status();
  wait(100);
  current_g = 0;
  current_b = 255;
  set_hw_status();
  wait(100);
  current_r = 255;
  current_g = 255;
  set_hw_status();
  wait(100);
  on_off_status = 0;
}


void loop()
{
  //En la primera iteracion del bucle incializamos la tira de leds y enviamos
  //su estado al gateway
  static bool first_message_sent = false;
  if ( first_message_sent == false ) {
    selftest();
    set_hw_status();
    send_status(1, 1, 1);
    first_message_sent = true;
  }
 
  //obtenemos en tiempo de ejecucion actuañ
  unsigned long now = millis();

  //si el tiempo pasado es mayor a la variable tick_leng actualizamos la variable
  //last_update con el valor del tiempo actual
  if (now - last_update > tick_length) {
    last_update = now;
    //si el modo de luces es desvaneciente llamamos una funcion que calcula el nivel
    //de desvanecimiento
    if (light_mode == LIGHT_FADING) {
      calc_fade();
    }
    //si algun modo que no sea el normal es activado se llama a la funcion handle_program
    //para ejecutar el programa seleccionado
    if (program_mode > PROGRAM_NORMAL) {
      handle_program();
    }
    
  }
  //Se actualiza el valor actual de cada led
  set_hw_status();
  
}
//Funcion para recibir mensajes del Gateway
void receive(const MyMessage &message)
{
  int val;
  //Si el mensaje es de tipo RGB
  if (message.type == V_RGB) {
    if (program_mode > PROGRAM_NORMAL) {
      stop_program();
    }
    Serial.print( "V_RGB: " );
    Serial.println(message.data);
    //capturmos el color que debe ser reprentado en la tira de leds
    long number = (long) strtol( message.data, NULL, 16);

    //guardamos en memoria el valor actual de la tira de leds
    strcpy(rgbstring, message.data);
    
    // Divimos la variable number para asignar los valores de cada color
    int r = number >> 16;
    int g = number >> 8 & 0xFF;
    int b = number & 0xFF;
    //iniciamos un desvanecimiento con el valor de color leido
    init_fade(fadespeed, r, g, b); 
    //se envia el valor actual de la tira
    send_status(0, 0, 1);
    //si el mensaje es de tipo V_light o de tipo V_STATUS
  } else if (message.type == V_LIGHT || message.type == V_STATUS) {
    //Si el numero de sensor es 2 se refiere al rele
    if(message.sensor == 2){
      state = !state;
      digitalWrite(RELAY_PIN, state ? RELAY_ON : RELAY_OFF);
      //guaradmos en memoria el valor actual de rele
      saveState(CHILD_ID_RELAY, state);
      
      Serial.print("Señal de conmutacion desde gateway, valor del rele cambiado a: ");
      Serial.println(message.getBool());
    }else{
      //Si el numero de sensor no es 2 se refiere a la tira de leds
      Serial.print( "V_LIGHT: " );
      Serial.println(message.data);
      val = atoi(message.data);
      //Apagamos o encendemos la tira de leds y enviamos un mensaje al gateway
      if (val == 0 or val == 1) {
        on_off_status = val;
        send_status(1, 0, 0);
      }
    }
    //Si el mensaje es de tipo V_DIMMER
  } else if (message.type == V_DIMMER || message.type == V_PERCENTAGE) {
    if (program_mode > PROGRAM_NORMAL) {
      stop_program();
    }
    //Leemos el valor de mensaje y cambiamos la intesidad de brillo de la tira de leds
    Serial.print( "V_DIMMER: " );
    Serial.println(message.data);
    val = atoi(message.data);
    if (val >= 0 and val <=100) {
      dimmerlevel = val;
      send_status(0, 1, 0);
    }
    //si el mensaje es de tipo V_VAR2 significa que se escogio un modo de luces
  } else if (message.type == V_VAR2 ) {
    Serial.print( "V_VAR2: " );
    Serial.println(message.data);
    val = atoi(message.data);
    if (val == PROGRAM_NORMAL) {
      stop_program();
    } else if (val == PROGRAM_ALARM || val == PROGRAM_RELAX || val == PROGRAM_ONLY_ONE) {
      //inicializamos el programa escogido
      init_program(val);
    }

  } else {
    Serial.println( "Invalid command received..." );
    return;
  }

}
//funcion que actualiza el valor de los leds de la tira RGB
void set_rgb(int r, int g, int b) {
  analogWrite(REDPIN, r);
  analogWrite(GREENPIN, g);
  analogWrite(BLUEPIN, b);
}

//Funcion que incializa el modo de luces
void init_program(int program) {
  program_mode = program;
  program_cycle = 0;
  save_rgb();
  
  if (program == PROGRAM_ALARM) {
    light_mode = LIGHT_NORMAL;
    current_r = 255;
    current_g = 255;
    current_b = 255;
    program_timer = 50;
  } else if (program == PROGRAM_RELAX) {
    program_timer = 300;
    init_fade(fadespeed,
              program_param_RELAX[program_cycle][0],
              program_param_RELAX[program_cycle][1],
              program_param_RELAX[program_cycle][2]);
  }else if (program == PROGRAM_ONLY_ONE) {
    light_mode = LIGHT_NORMAL;
    current_r = 0;
    current_g = 0;
    current_b = 0;
    program_timer = 50;
  }
}

//Funcion que actualiza el estado del modo de luces
void handle_program() {
  program_timer--;
  if (program_mode == PROGRAM_ALARM) {
    if (program_timer == 0) {
      program_timer = 50;
      if (program_cycle == 0) {
        program_cycle = 1;
        current_r = 0;
        current_g = 0;
        current_b = 0;
      } else {
        program_cycle = 0;
        current_r = 255;
        current_g = 255;
        current_b = 255;
      }
    }
  } else if (program_mode == PROGRAM_RELAX) {
    if (light_mode == LIGHT_NORMAL) {
      program_cycle = (program_cycle+1) % MAX_CYCLES_RELAX;
      Serial.print("Next cycle step ");
      Serial.println(program_cycle);
      init_fade(fadespeed * RELAX_SPEED,
                program_param_RELAX[program_cycle][0],
                program_param_RELAX[program_cycle][1],
                program_param_RELAX[program_cycle][2]);
    
    }
    
  }else if (program_mode == PROGRAM_ONLY_ONE) {
    if (program_timer == 0) {
      program_timer = 50;
      if (cont_program_only_one == 0) {
        cont_program_only_one++;
        current_r = 255;
        current_g = 0;
        current_b = 0;
      } else if (cont_program_only_one == 1) {
        cont_program_only_one++;
        current_r = 0;
        current_g = 255;
        current_b = 0;
      }else{
        cont_program_only_one++;
        current_r = 0;
        current_g = 0;
        current_b = 255;
      }
      if(cont_program_only_one>2)
        cont_program_only_one=0;
    }
  }
}

//funcion que detiene la ejecucion de un modo de luces
void stop_program() {
  restore_rgb();
  light_mode = LIGHT_NORMAL;
  program_mode = PROGRAM_NORMAL;
}
//funcion que guarda los valores de color de la tira rgb
void save_rgb() {
  save_r = current_r;
  save_g = current_g;
  save_b = current_b;
}
//funcion restaura los colores en la tira RGB
void restore_rgb() {
  current_r = save_r;
  current_g = save_g;
  current_b = save_b;
}
//funcion que incializa el desvanecimiento de los leds 
void init_fade(int t, int r, int g, int b) {
  Serial.print( "Init fade" );
  light_mode = LIGHT_FADING;
  target_r = r;
  target_g = g;
  target_b = b;
  fade_step = t;
  delta_r = (target_r - current_r) / float(fade_step);
  delta_g = (target_g - current_g) / float(fade_step);
  delta_b = (target_b - current_b) / float(fade_step);
}
//funcion que actualiza el desvanecimiento de los leds
void calc_fade() {
  if (fade_step > 0) {
    fade_step--;
    current_r = target_r - delta_r * fade_step;
    current_g = target_g - delta_g * fade_step;
    current_b = target_b - delta_b * fade_step;
  } else {
    Serial.println( "Normal mode" );
    light_mode = LIGHT_NORMAL;
  } 
}

//Funcion que actualiza el valor de los colores de la tira RGB
void set_hw_status() {
  int r = on_off_status * (int)(current_r * dimmerlevel/100.0);
  int g = on_off_status * (int)(current_g * dimmerlevel/100.0);
  int b = on_off_status * (int)(current_b * dimmerlevel/100.0);

  set_rgb(r, g, b);
}
//Funcion que envia los mensajes al gateway, Sus parametros de entrada indican
//que tipo de mensaje va a enviar
void send_status(int send_on_off_status, int send_dimmerlevel, int send_rgbstring) {
  if (send_rgbstring) send(rgbMsg.set(rgbstring));
  if (send_on_off_status) send(lightMsg.set(on_off_status));
  if (send_dimmerlevel) send(dimmerMsg.set(dimmerlevel));
}
